import { defineConfig } from 'vite'
import sassGlobImports from 'vite-plugin-sass-glob-import'

export default defineConfig({
  plugins: [
    sassGlobImports()
  ],
  build: {
    watch: true,
    outDir: 'theme/assets',
    emptyOutDir: false,
    lib: {
      entry: 'src/main.js',
      name: 'main',
      formats: ['umd'],
      fileName: () => 'main.js'
    }
  }
})

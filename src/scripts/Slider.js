import Swiper, { Lazy, Navigation, Pagination, Thumbs } from 'swiper';

import 'swiper/scss';
import 'swiper/scss/lazy';
import 'swiper/scss/navigation';
import 'swiper/scss/pagination';
import 'swiper/scss/thumbs';

class Slider {
  constructor() {
    const homeHeroSlider = new Swiper('.home-hero__slider .swiper', {
      modules: [
        Lazy,
        Navigation,
        Pagination
      ],
      slidesPerView: 1,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        prevEl: '.home-hero__slider .swiper-button-prev',
        nextEl: '.home-hero__slider .swiper-button-next'
      },
      pagination: {
        el: '.home-hero__slider .swiper-pagination',
        clickable: true
      }
    });

    const homeProductsSlider = new Swiper('.home-products__slider .swiper', {
      modules: [
        Lazy,
        Navigation
      ],
      slidesPerView: 1,
      spaceBetween: 40,
      loop: true,
      preloadImages: false,
      lazy: true,
      watchSlidesProgress: true,
      navigation: {
        prevEl: '.home-products__slider .swiper-button-prev',
        nextEl: '.home-products__slider .swiper-button-next'
      },
      breakpoints: {
        600: {
          slidesPerView: 2,
        },
        900: {
          slidesPerView: 3
        },
        1200: {
          slidesPerView: 4
        }
      }
    });

    const productHeroThumbs = new Swiper('.product-hero__thumbs .swiper', {
      modules: [
        Lazy
      ],
      slidesPerView: 1,
      spaceBetween: 25,
      autoHeight: true,
      loop: true,
      centeredSlides: true,
      preloadImages: false,
      lazy: true,
      watchSlidesProgress: true,
      breakpoints: {
        600: {
          slidesPerView: 'auto',
          direction: 'vertical'
        }
      }
    });

    const productHeroSlider = new Swiper('.product-hero__slider .swiper', {
      modules: [
        Lazy,
        Navigation,
        Pagination,
        Thumbs
      ],
      slidesPerView: 1,
      spaceBetween: 40,
      autoHeight: true,
      loop: true,
      preloadImages: false,
      lazy: true,
      thumbs: {
        swiper: productHeroThumbs
      },
      navigation: {
        prevEl: '.product-hero__thumbs .swiper-button-prev',
        nextEl: '.product-hero__thumbs .swiper-button-next'
      },
      pagination: {
        el: '.product-hero__slider .swiper-pagination',
        clickable: true
      }
    });

    const productRelatedSlider = new Swiper('.product-related__slider .swiper', {
      modules: [
        Lazy,
        Navigation
      ],
      slidesPerView: 1,
      spaceBetween: 40,
      loop: true,
      preloadImages: false,
      lazy: true,
      watchSlidesProgress: true,
      navigation: {
        prevEl: '.product-related__slider .swiper-button-prev',
        nextEl: '.product-related__slider .swiper-button-next'
      },
      breakpoints: {
        600: {
          slidesPerView: 2,
        },
        900: {
          slidesPerView: 3
        },
        1200: {
          slidesPerView: 4
        }
      }
    });
  }
}

export default new Slider;

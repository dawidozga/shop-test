import Lazy from './lazy';

class Variant {
  constructor() {
    const optionRadios = document.querySelectorAll('.variant__option__radio');
    const optionLists = document.querySelectorAll('.variant__option__list');

    optionRadios.forEach(el => {
      el.addEventListener('change', this.changeVariant);
    });

    optionLists.forEach(el => {
      el.addEventListener('change', this.changeVariant);
    });
  }

  async changeVariant(e) {
    const variant = document.querySelector('.variant');
    const variantFormButton = document.querySelector('.variant__form__button');

    variantFormButton.disabled = true;

    const url = new URL(location);

    url.searchParams.set('variant', e.currentTarget.value);
    history.pushState({}, document.title, url);

    const response = await fetch(url);
    const data = await response.text();

    const parser = new DOMParser;
    const html = parser.parseFromString(data, 'text/html');

    variant.innerHTML = html.querySelector('.variant').innerHTML;

    new Variant;
    Lazy.update();
  }
}

export default new Variant;
